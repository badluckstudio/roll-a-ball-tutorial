﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject Player;

	private Vector3 offSet;

	// Use this for initialization
	void Start () {
		offSet = transform.position - Player.transform.position;
	}
	
	// LateUpdate is called once per frame after all object has been set
	void LateUpdate () {
		transform.position = Player.transform.position + offSet;
	}
}
